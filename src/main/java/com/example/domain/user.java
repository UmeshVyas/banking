package com.example.domain;

public class user {

	int userid;
	String nameOfUser;
	String accountType;
	int balance;
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getNameOfUser() {
		return nameOfUser;
	}
	public void setNameOfUser(String nameOfUser) {
		this.nameOfUser = nameOfUser;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	public user( String nameOfUser, String accountType, int balance) {
		super();
		
		this.nameOfUser = nameOfUser;
		this.accountType = accountType;
		this.balance = balance;
	}
	
	
}
