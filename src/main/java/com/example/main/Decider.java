package com.example.main;

import java.util.Scanner;

import org.omg.CORBA.portable.ValueOutputStream;

import rx.Observable;
import rx.Subscriber;
import rx.Observable.OnSubscribe;

import com.example.domain.user;

public class Decider {

	String t;
	int ch;
	Scanner sc=new Scanner(System.in);
	int trans_amount=0;
	String name = null;
	String ac_type=null;
	//public Integer balance=0;

	@SuppressWarnings("deprecation")
	public Integer wantTrans(Integer balance){
		
		 
		Observable<Integer>obs=Observable.create(new OnSubscribe<Integer>() {
				
			public void call(Subscriber<? super Integer> sub) {
				
				System.out.println("You want any transactions ? (Yy/Nn)");
				t=sc.next();
				//int bal=Integer.parseInt(balance.toString());
				int bal=balance.intValue();
				if(t.startsWith("N") || t.startsWith("n")){
					
					System.out.println("Thanks for visit \n Your Balance is : "+bal);
					
				}else if(t.startsWith("Y") || t.startsWith("y")){
					System.out.println("Enter Your choice : \n");
					System.out.println("1. Deposit Amount\n");
					System.out.println("2. Withdrawl Amount \n");
					System.out.println("3. See Current Balance \n");
					
					System.out.println("Press any other key for exit \n");
					ch=sc.nextInt();
						switch (ch) {
					case 1:
						System.out.println("Enter amount for deposit \n");
						trans_amount=sc.nextInt();
						bal=bal+trans_amount;
						System.out.println("After adding "+trans_amount+" rupee in your account, Your balance is :"+bal);
						trans_amount=0;
						wantTrans(bal);
						break;

					case 2:
						System.out.println("Enter amount for withdraw \n");
						trans_amount=sc.nextInt();
						if(trans_amount>bal){
							System.out.println("Withdrawl amount is higher than balance please try again \n");
							wantTrans(bal);
						}else{
							
							bal=bal-trans_amount;
							System.out.println("After deducting "+trans_amount+" rupee in your account, Your balance is :"+bal);
							trans_amount=0;
							wantTrans(bal);
						}
						break;
					case 3:
						System.out.println("YOUR CURRENT BALANCE IS :"+bal);
						wantTrans(bal);
						break;
					
					default:
						System.exit(0);
						break;
					}
				}else{
					
					System.exit(0);
				}
			//	balance=bal;
			}
		});
		Subscriber<Integer>sub=new Subscriber<Integer>() {
			
			@Override
			public void onNext(Integer t) {
				System.out.println("Balance ::::"+t);
			}
			
			@Override
			public void onError(Throwable e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onCompleted() {
				// TODO Auto-generated method stub
				
			}
		};
		System.out.println("BALANCE :::::"+balance);
		obs.subscribe(sub);
		return balance;
	}
	
}
